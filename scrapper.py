import requests  
r = requests.get('https://www.complaintboard.in/complaints-reviews/passport-seva-l270322/page/10')
print(r.status_code)
from bs4 import BeautifulSoup  
soup = BeautifulSoup(r.text, 'html.parser')  
results = soup.find_all(class_='complaint')

records = []  
for result in results:  
    cont = result.find('h4').text[1:-1]
    records.append((cont))
    cont2 = result.find('div').text[1:-1]
    records.append((cont2))

import pandas as pd  
df = pd.DataFrame(records, columns=['cont' 'cont2'])   
df.to_csv('psk2_10.csv', index=False, encoding='utf-8') 