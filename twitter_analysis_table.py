import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
import collections

import tweepy as tw
import nltk
from nltk.corpus import stopwords
import re
import networkx

import warnings
warnings.filterwarnings("ignore")

sns.set(font_scale=1.5)
sns.set_style("whitegrid")

access_token = '272417905-AyYXoGnZejPIIRzBMoPZgMtUiVyVAjbiqikj642T'
access_token_secret = 'sorkliVafmXJs8pMDE0RUXPFmxJG4tzYfzqKIlnUiEmyo'
consumer_key = 'nHa8hIRZrxhubCBl2GaiJq2jG'
consumer_secret = 'IV3Rrnd2yCEX32FPuQU7CmAXdEvLPzqsNPh1SpALTyBUCB4vFx'

auth = tw.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tw.API(auth, wait_on_rate_limit=True)

search_words = "PSK", "passport"
date_since = "2018-12-01"

tweets = tw.Cursor(api.search,
              q=search_words,
              lang="en",
              since=date_since, tweet_mode='extended').items(100000)
#print(tweets)

# for tweet in tweets:
#    print(tweet.text)

all_tweets = [tweet.full_text for tweet in tweets]

all_tweets[:10000]

def remove_url(txt):
    """Replace URLs found in a text string with nothing 
    (i.e. it will remove the URL from the string).

    Parameters
    ----------
    txt : string
        A text string that you want to parse and remove urls.

    Returns
    -------
    The same txt string with url's removed.
    """

    return " ".join(re.sub("([^0-9A-Za-z \t])|(\w+:\/\/\S+)", "", txt).split())
all_tweets_no_urls = [remove_url(tweet) for tweet in all_tweets]
all_tweets_no_urls[:10000]

words_in_tweet = [tweet.lower().split() for tweet in all_tweets_no_urls]
words_in_tweet[:10000]


nltk.download('stopwords')

stop_words = set(stopwords.words('english'))

# all_words_no_urls = list(itertools.chain(*words_in_tweet))

# counts_no_urls = collections.Counter(all_words_no_urls)

# counts_no_urls.most_common(50)

####
for all_words in words_in_tweet:
    for word in all_words:
        tweets_nsw = [[word for word in tweet_words if not word in stop_words] for tweet_words in words_in_tweet]

all_words_nsw = list(itertools.chain(*tweets_nsw))

counts_nsw = collections.Counter(all_words_nsw)

counts_nsw.most_common(50)
####

clean_tweets_nsw = pd.DataFrame(counts_nsw.most_common(50), columns=['words', 'count'])
clean_tweets_nsw.to_csv('frequencyOfTwitterWords.csv', sep='\t', encoding='utf-8')


